mixer = mixitup('#mix-wrapper', {
    animation: {
    effects: 'fade rotateZ(-180deg)',
    duration: 700
  },
  classNames: {
    block: 'programs',
    elementFilter: 'filter-btn',
    elementSort: 'sort-btn'
  },
  selectors: {
    target: '.mix-target'
  }
});

function change(pos1, pos2){
	
	var l1 = document.getElementById("l"+pos1);
	var l2 = document.getElementById("l"+pos2);
	l1.setAttribute("data-order", pos2);
	l2.setAttribute("data-order", pos1);
	console.log(l1);
	console.log(l2);
	
//	l1.setAttribute("style", "background-color: red;")
	mixer.forceRefresh();
	mixer.sort('order1:asc')
	.then(function(state) {
		console.log(state.activeSort.order === 'asc'); // true
	});
	mixer.sort('order:asc')
    .then(function(state) {
        console.log(state.activeSort.order === 'asc'); // true
    });
	
}

function connect(){
	$.ajax({
		url: "/HipstersSelectionSortWeb/connector",
		type: "get",
		cache: false,
		success: function(data) {
			console.log(data);
		}
	});
	
	function consoleConnector(data){
		setTimeout(function(){
			var consoleId = document.getElementById("consoleId");
			if(data)
				consoleId.value += data;
			
			$.ajax({
				url: "/HipstersSelectionSortWeb/pagehandler",
				type: "get",
				cache: false,
				success: consoleConnector
			});
		}, 1000);
	}
	function update(data){
		setTimeout(function(){
			if(data){
				var parsed = JSON.parse(data);
	
				var consoleId = document.getElementById("consoleId");
				if(parsed['tacho'])
					consoleId.value += "distance traveled:" + parsed['tacho'] + "\n";

				console.log(parsed);
				if(parsed["0"]){
					var test = document.getElementById("l1");
					var test2 = document.getElementById("a1");
					test2.innerHTML  = Number(parsed["0"].number);
					test.style.backgroundColor = "rgb("+parsed["0"].red + "," + parsed["0"].green + "," + parsed["0"].blue + ")";
				}
				if(parsed["1"]){
					var test = document.getElementById("l2");
					var test2 = document.getElementById("a2");
					test2.innerHTML  = Number(parsed["1"].number);
					test.style.backgroundColor = "rgb("+parsed["1"].red + "," + parsed["1"].green + "," + parsed["1"].blue + ")";
				}
				if(parsed["2"]){
					var test = document.getElementById("l3");
					var test2 = document.getElementById("a3");
					test2.innerHTML  = Number(parsed["2"].number);
					test.style.backgroundColor = "rgb("+parsed["2"].red + "," + parsed["2"].green + "," + parsed["2"].blue + ")";
				}
				if(parsed["3"]){
					var test = document.getElementById("l4");
					var test2 = document.getElementById("a4");
					test2.innerHTML  = Number(parsed["3"].number);
					test.style.backgroundColor = "rgb("+parsed["3"].red + "," + parsed["3"].green + "," + parsed["3"].blue + ")";
				}
				if(parsed["4"]){
					var test = document.getElementById("l5");
					var test2 = document.getElementById("a5");
					test2.innerHTML  = Number(parsed["4"].number);
					test.style.backgroundColor = "rgb("+parsed["4"].red + "," + parsed["4"].green + "," + parsed["4"].blue + ")";
				}
				if(parsed["5"]){
					var test = document.getElementById("l6");
					var test2 = document.getElementById("a6");
					test2.innerHTML  = Number(parsed["5"].number);
					test.style.backgroundColor = "rgb("+parsed["5"].red + "," + parsed["5"].green + "," + parsed["5"].blue + ")";
					console.log(parsed);
				}
			}
			
			$.ajax({
				url: "/HipstersSelectionSortWeb/connector?please=update",
				type: "post",
				cache: false,
				success: update
			});
		}, 1000);
	}
	
	update();
	consoleConnector();
}

function sort(){
	change('4','1');
	setTimeout(function(){change('3','2');},2000);
	setTimeout(function(){change('2','1');},4000);
	setTimeout(function(){change('5','2');},6000);
}

function start(){
	$.ajax({
		url: "/HipstersSelectionSortWeb/connector?command=start",
		type: "post",
		cache: false
	});
}