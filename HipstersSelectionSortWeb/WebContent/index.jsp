<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/sao.css">
<script type="text/javascript" src="./libs/mixitup-3/dist/mixitup.min.js"></script>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Hipsters Selection Sort</title>
</head>
<body>
	<f:view>
	<div>
		<div class="menu" style="">
			<ul class="menu-list">
				<li class="menu-list-item" onclick="connect()">Connect</li>
				<li class="menu-list-item" onclick="start()">Start</li>
				<li class="menu-list-item" onclick="sort()">Sort</li>
				<li class="menu-list-item">Logout</li>
			</ul>
		</div>
		<textarea style="display:inline-block; width:60%; height: 120px;" id="consoleId" rows="4" cols="50"></textarea>
	
	<!-- /div>
		<br>
		<div style="margin: 0 39%">
			<div id=b1
				style="margin: 5px; display: inline-block; width: 30px; height: 30px; outline: white solid thin;"></div>
			<div id=b2
				style="margin: 5px; display: inline-block; width: 30px; height: 30px; outline: white solid thin;"></div>
			<div id=b3
				style="margin: 5px; display: inline-block; width: 30px; height: 30px; outline: white solid thin;"></div>
			<div id=b4
				style="margin: 5px; display: inline-block; width: 30px; height: 30px; outline: white solid thin;"></div>
			<div id=b5
				style="margin: 5px; display: inline-block; width: 30px; height: 30px; outline: white solid thin;"></div>
		</div-->
		
	<div class="wrap">
  		<div class="container">


    <div class="row">
      <div class="twelve columns">
        <h4>Numbers</h4>
      </div>
    </div><!-- end of row -->

    <div class="row">
      <div class="twelve columns">
        <ul class="courses" id="mix-wrapper">
          <li id="l1" class="mix-target" data-order="1"><a id='a1' href="#">-</a></li>
          <li id="l2" class="mix-target" data-order="2"><a id='a2' href="#">-</a></li>
          <li id="l3" class="mix-target" data-order="3"><a id='a3' href="#">-</a></li>
          <li id="l4" class="mix-target" data-order="4"><a id='a4' href="#">-</a></li>
          <li id="l5" class="mix-target" data-order="5"><a id='a5' href="#">-</a></li>
        </ul>
      </div>
    </div><!-- end of row -->   
  </div><!-- end of container -->
</div><!-- end of wrap -->
<script type="text/javascript" src="./js/buttonHandler.js"></script>
<footer>
  <p class="p">Hss-wiesloch</p>
</footer>
		
		
</f:view>
</body>
</html>