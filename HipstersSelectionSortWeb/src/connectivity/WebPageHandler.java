package connectivity;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/pagehandler")
public class WebPageHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static ArrayList<String> consoleEntries = new ArrayList<String>();
       
    public WebPageHandler() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		for(int i = 0; i < consoleEntries.size(); i++){
			response.getWriter().println(consoleEntries.get(i));
		}
		consoleEntries.clear();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("hi");
	}

	public static void logWeb(String string){
		consoleEntries.add(string);
	}
	
}
