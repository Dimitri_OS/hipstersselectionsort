package connectivity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.net.ConnectException;
import java.net.Socket;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Connector
 */
@WebServlet("/connector")
public class Connector extends HttpServlet {
	
	private Socket socket = null;
	private ObjectOutputStream outputStream = null;
	private ObjectInputStream inputStream = null;
	private static final long serialVersionUID = 1L;

	public Connector() throws IOException {
		super();
	}

	public void init(ServletConfig config) throws ServletException {
		try {
			socket = new Socket("10.97.112.3", 4567);
			outputStream = new ObjectOutputStream(socket.getOutputStream());
			inputStream = new ObjectInputStream(socket.getInputStream());
			Thread iHandler = new Thread(new InputHandler(inputStream));
			iHandler.start();
			System.out.println("Connected");
		} catch (ConnectException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		WebPageHandler.logWeb("Connection status:"
				+ (socket != null ? (socket.isConnected() ? "true" : "false")
						: " false"));
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("command") != null
				&& !"".equals(request.getParameter("command"))) {
			WebPageHandler.logWeb("command:" + request.getParameter("command"));
			outputStream.writeObject(request.getParameter("command"));
		}
		if (request.getParameter("please") != null
				&& !"".equals(request.getParameter("please"))) {
			String json = "{";
			for (int i = 0; i < InputHandler.colors.length; i++) {
				if(InputHandler.colors[i]!=null)
					json=json.concat("\"" + String.valueOf(i)+ "\"" + ":{\"red\":"
						+ InputHandler.colors[i].getRed() + ",\"green\":"
						+ InputHandler.colors[i].getGreen() + ",\"blue\":"
						+ InputHandler.colors[i].getBlue() + ",\"number\":"
						+ InputHandler.numbers[i] + "}"
						+ ((i < InputHandler.colors.length-1) && InputHandler.colors[i+1]!=null ? "," : ""));
			}
			json=json.concat((json.length()==1 ? "" : ",") + "\"tacho\":"+ InputHandler.tacho);
			json=json.concat("}");
			response.getWriter().write(json);
		}
	}

}
