package connectivity;

import java.awt.Color;
import java.io.IOException;
import java.io.ObjectInputStream;

public class InputHandler implements Runnable {

	private ObjectInputStream inputStream = null;

	static Color[] colors = new Color[6];
	static int[] numbers = new int[5];
	static double tacho = 0;

	public InputHandler(ObjectInputStream inputStream) {
		setInputStream(inputStream);
	}

	@Override
	public void run() {

		String command = "init";

		while ("exit".equals(command) == false) {
			Object readData = null;

			try {
				readData = getInputStream().readObject();
				if (readData instanceof String) {
					command = (String) readData;
					handleCommand(command);
				}
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
				command = "exit";
			}
		}

	}

	private void handleCommand(String command) {
		System.out.println("Handling command_".concat(command).split(":")[0]);
		switch (command.split(":")[0]) {
		case "color":
			numbers[Integer.parseInt(command.split(":")[1])-1] = Integer.parseInt(command.split(":")[2]);
			colors[Integer.parseInt(command.split(":")[1])-1] = new Color(
				    Math.min(Integer.parseInt(command.split(":")[3]),255),
				    Math.min(Integer.parseInt(command.split(":")[4]),255),
		    		Math.min(Integer.parseInt(command.split(":")[5]),255));
		case "tacho":
			tacho = Double.parseDouble(command.split(":")[1]);
			System.out.println(tacho);
			break;
		default:
			System.out.println(command);
			WebPageHandler.logWeb(command);
			break;
		}
	}

	// Commands

	public void setInputStream(ObjectInputStream inputStream) {
		this.inputStream = inputStream;
	}

	public ObjectInputStream getInputStream() {
		return inputStream;
	}

}
