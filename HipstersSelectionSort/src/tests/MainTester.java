package tests;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import Handlers.InputHandler;
import Handlers.OutputHandler;
import Handlers.SelectionSort;

public class MainTester {
	
	static OutputHandler outputHandler;
	
	public static void main(String[] args) throws IOException, InterruptedException {
		
		ServerSocket server = new ServerSocket(4567);
		System.out.println("Server started");
		server.setSoTimeout(100000);
//		server.setSoTimeout(1);
		
		try {
			Socket client = server.accept();
			System.out.println("Client connected");
			
			ObjectInputStream inputStream = new ObjectInputStream(client.getInputStream());
			ObjectOutputStream outputStream = new ObjectOutputStream(client.getOutputStream());
			
			initInput(inputStream);
			initOutput(outputStream);
			
			server.close();
		} catch (Exception e) {
			new SelectionSort().init();
		}
	}
	
	public static void initInput(ObjectInputStream inputStream){
		
		Thread thread = new Thread(new InputHandler(inputStream));
		thread.start();
		
	}
	
	public static void initOutput(ObjectOutputStream outputStream){
		
		setOutputHandler(new OutputHandler(outputStream));
	
	}
	
	public static void sendData(Object object) {
		getOutputHandler().sendData(object);
	}
	
	public static void sendLog(String log) {
		if(getOutputHandler()!=null)
			getOutputHandler().sendData("log:".concat(log));
	}
	
	public static void setOutputHandler(OutputHandler outputHandler) {
		MainTester.outputHandler = outputHandler;
	}
	public static OutputHandler getOutputHandler() {
		return outputHandler;
	}
	
	
	
	

}
