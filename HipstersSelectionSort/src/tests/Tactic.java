package tests;

import java.util.ArrayList;
import java.util.List;

public abstract interface Tactic {
	
	List<Integer> args = new ArrayList<>();

	abstract void act();
	
}
