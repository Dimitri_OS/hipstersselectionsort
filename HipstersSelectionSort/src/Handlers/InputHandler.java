package Handlers;

import java.io.IOException;
import java.io.ObjectInputStream;

import tests.MainTester;

public class InputHandler implements Runnable {

	private ObjectInputStream inputStream = null;

	public InputHandler(ObjectInputStream inputStream) {
		setInputStream(inputStream);
	}

	@Override
	public void run() {

		String command = "init";

		while ("exit".equals(command) == false) {
			Object readData = null;

			try {
				readData = getInputStream().readObject();
				if (readData instanceof String) {
					command = (String) readData;
					handleCommand(command);
				}
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
				command = "exit";
			}
		}

	}

	private void handleCommand(String command) {
		switch (command) {
		case "init":
			init();
			break;
		case "start":
			start();
			break;
		case "ls":
			ls();
			break;
		case "ping":
			ping();
			MainTester.sendData("pong");
			break;
		default:
			System.out.println(command);
			break;
		}
	}

	// Commands
	private void init() {
		System.out.println("Initialized.");
	}

	private void start() {
		System.out.println("Started.");
		SelectionSort selectionSort = new SelectionSort();
		selectionSort.init();
	}

	private void ls() {
		System.out.println("/bin");
		System.out.println("/data");
		System.out.println("/dev");
	}

	private void ping() {
		System.out.println("Ping received.");
	}

	public void setInputStream(ObjectInputStream inputStream) {
		this.inputStream = inputStream;
	}

	public ObjectInputStream getInputStream() {
		return inputStream;
	}

}
