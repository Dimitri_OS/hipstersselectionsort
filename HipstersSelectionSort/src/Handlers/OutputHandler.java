package Handlers;

import java.io.IOException;
import java.io.ObjectOutputStream;

public class OutputHandler {

	private ObjectOutputStream outputStream = null;

	public OutputHandler(ObjectOutputStream outputStream) {
		setOutputStream(outputStream);
	}

	public void sendData(Object object) {
		try {
			outputStream.writeObject(object);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setOutputStream(ObjectOutputStream outputStream) {
		this.outputStream = outputStream;
	}

	public ObjectOutputStream getOutputStream() {
		return outputStream;
	}

	public void sendColor(int position, int sortNumber, int red, int green, int blue) {
		if(this.outputStream!=null)
		try {
			this.outputStream.writeObject("color:" + position + ":" + sortNumber + ":" + red + ":" + green + ":" + blue);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendLog(String log) {
		if(this.outputStream!=null)
		try {
			this.outputStream.writeObject("log:".concat(log));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendTacho(String distance) {
		try {
			this.outputStream.writeObject("tacho:" + distance);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
