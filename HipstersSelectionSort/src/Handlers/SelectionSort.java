package Handlers;

import java.text.DecimalFormat;
import java.util.HashMap;

import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.robotics.Color;
import lejos.robotics.RegulatedMotor;
import tests.MainTester;
import tests.Tactic;

public class SelectionSort {

	// Finals
	public static final int TACTIC_NORMAL = 0;
	public static final int TACTIC_GO_BACK = 1;
	private static final int TACTIC_TURN_AROUND = 2;
	public static final int TACTIC_TRANSPORT = 3;
	public static final int TACTIC_SKIP_POSITION = 4;

	public static final int FORWARD = 1;
	public static final int STOP = 0;
	public static final int BACKWARD = -1;

	public static final int NUMBEROFBLOCKS = 5;

	// Vehicle vars
	static int speedLeft = 0;
	static int speedRight = 0;
	static int directionMotorLeft = 0;
	static int directionMotorRight = 0;
	static HashMap<Integer, Tactic> tactics = new HashMap<>();
	static int currentTactic = 0;
	static int position = 0;
	static int positionOfNextBlock = 3;
	static int dirtyBlueCounter = 0;

	// Motoren
	EV3LargeRegulatedMotor motorLeft = new EV3LargeRegulatedMotor(MotorPort.A);
	EV3LargeRegulatedMotor motorRight = new EV3LargeRegulatedMotor(MotorPort.C);
	EV3LargeRegulatedMotor shovel = new EV3LargeRegulatedMotor(MotorPort.B);

	// Touchsensor
	EV3TouchSensor touchSensor = new EV3TouchSensor(SensorPort.S1);

	// Farbsensor
	EV3ColorSensor colorSensorFront = new EV3ColorSensor(SensorPort.S2);
	EV3ColorSensor colorSensorBottom = new EV3ColorSensor(SensorPort.S3);

	// Samples
	float[] touchSensorArray;
	float[] colorSensorArray;
	float[] colorSensorFrontArray;

	// Threads
	Thread runThread;
	Thread stopThread;
	Thread readRGB;

	public void init() {
		colorSensorBottom.setCurrentMode("RGB");
		colorSensorFront.setCurrentMode("RGB");
		touchSensor.setCurrentMode("Touch");

		colorSensorBottom.setFloodlight(false);
		colorSensorBottom.setFloodlight(Color.BLUE);
		colorSensorFront.setFloodlight(false);
		colorSensorFront.setFloodlight(Color.BLUE);

		touchSensorArray = new float[touchSensor.sampleSize()];
		colorSensorArray = new float[colorSensorBottom.sampleSize()];
		colorSensorFrontArray = new float[colorSensorBottom.sampleSize()];

		tactics.put(TACTIC_NORMAL, new Tactic() {
			@Override
			public void act() {

				colorSensorBottom.fetchSample(colorSensorArray, 0);
				colorSensorFront.fetchSample(colorSensorFrontArray, 0);

				if (colorSensorArray[0] * 1000 < 50 & colorSensorArray[1] * 1000 < 50
						& colorSensorArray[2] * 1000 < 50) {
					// black
					goRight();
				}

				if (colorSensorArray[0] * 1000 > 200 & colorSensorArray[1] * 1000 < 50
						& colorSensorArray[2] * 1000 < 50) {
					// red
					setPosition();
					scanFront();
					currentTactic = TACTIC_TRANSPORT;
					tactics.get(currentTactic).args.add(1);
				}

				if (colorSensorArray[0] * 1000 < 70 & colorSensorArray[1] * 1000 < 70
						& colorSensorArray[2] * 1000 > 70) {

//					increaseBlueCounterAndCheckPosition();
					goRight();
				}

				if (colorSensorArray[0] * 1000 > 200 & colorSensorArray[1] * 1000 > 200
						& colorSensorArray[2] * 1000 > 100) {
					// white
					goLeft();
				}

				// LCD.drawString("Tacho: " + getDistance() + " cm", 0, 1);

				timeoutInMs(1);

			}

		});
		tactics.put(TACTIC_GO_BACK, new Tactic() {
			@Override
			public void act() {

				move(150, 150, BACKWARD);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					currentTactic = TACTIC_TURN_AROUND;
				}

			}
		});
		tactics.put(TACTIC_TRANSPORT, new Tactic() {
			@Override
			public void act() {
				move(150, 150, BACKWARD);
				timeoutInMs(1000);
				
				move(180, 160, BACKWARD, FORWARD);
				timeoutInMs(2000);
				
				currentTactic = TACTIC_NORMAL;
			}
		});
		tactics.put(TACTIC_TURN_AROUND, new Tactic() {
			@Override
			public void act() {

				move(180, 160, BACKWARD, FORWARD);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					currentTactic = TACTIC_NORMAL;
				}

			}
		});
		tactics.put(TACTIC_SKIP_POSITION, new Tactic() {
			@Override
			public void act() {

				move(100, 100, FORWARD);
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					currentTactic = TACTIC_NORMAL;
				}

			}
		});

		runThread = new Thread(new Runnable() {
			@Override
			public void run() {

				while (true) {

					LCD.drawString("Tacho: " + getDistance() + " cm", 0, 4);
					MainTester.getOutputHandler().sendTacho((getDistance()));
					
					// LCD.drawString("#blue: " + getDirtyBlueCounter(), 0, 6);
					// LCD.drawString("#posi: " + position, 0, 7);

					// LCD.drawString("#red: " + colorSensorFrontArray[0] *
					// 1200, 0, 5);
					// LCD.drawString("#green: " + colorSensorFrontArray[1] *
					// 1200, 0, 6);
					// LCD.drawString("#blue: " + colorSensorFrontArray[2] *
					// 1200, 0, 7);

					// LCD.drawString("Number: " + getSortNumber(), 0, 6);

					motorLeft.setSpeed(speedLeft);
					motorRight.setSpeed(speedRight);

					if (directionMotorLeft == BACKWARD) {
						motorLeft.backward();
					} else if (directionMotorLeft == FORWARD) {
						motorLeft.forward();
					} else {
						motorLeft.stop();
					}

					if (directionMotorRight == BACKWARD) {
						motorRight.backward();
					} else if (directionMotorRight == FORWARD) {
						motorRight.forward();
					} else {
						motorRight.stop();
					}

//					try {
//						Thread.sleep(2);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
					timeoutInMs(2);

				}
			}

		});

		stopThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					touchSensor.fetchSample(touchSensorArray, 0);
					if (touchSensorArray[0] == 1.0f) {
						shovel.rotateTo(0);
						Sound.beepSequenceUp();
						System.exit(-1);
					}
				}
			}
		});

		readRGB = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					tactics.get(currentTactic).act();
				}
			}

		});

		stopThread.start();
		readRGB.start();
		runThread.start();
	}

	private void checkPositionAndActAccordingly() {

		if (position == positionOfNextBlock) {
			goGetThatBlock();
		} else {
			currentTactic = TACTIC_SKIP_POSITION;
		}

	}

	private String getDistance() {
		DecimalFormat df = new DecimalFormat("0.00");
		return df.format(motorLeft.getPosition() / 360 * 17.6);
	}

	private String getDirtyBlueCounter() {
		DecimalFormat df = new DecimalFormat("00");
		return df.format(dirtyBlueCounter);
	}

	private int getSortNumberValue(int position) {

		int number = 0;
		
		switch (position) {
		case 1:
			number = 3;
			break;
		case 2:
			number = 5;
			break;
		case 3:
			number = 2;
			break;
		case 4:
			number = 1;
			break;
		case 5:
			number = 4;
			break;
		default:
			break;
		}
		
		return number;

		// // green
		// if (colorSensorFrontArray[0] * 1200 < 70 & colorSensorFrontArray[1] *
		// 1200 > 150
		// & colorSensorFrontArray[2] * 1200 < 50) {
		// number = 1;
		// }
		//
		// // orange
		// if (colorSensorFrontArray[0] * 1200 < 135 & colorSensorFrontArray[1]
		// *
		// 1200 < 30
		// & colorSensorFrontArray[2] * 1200 < 20) {
		// number = 2;
		// }
		//
		// // pink
		// if (colorSensorFrontArray[0] * 1200 < 180 & colorSensorFrontArray[1]
		// *
		// 1200 < 50
		// & colorSensorFrontArray[2] * 1200 < 50) {
		// number = 3;
		// }
		//
		// // red
		// if (colorSensorFrontArray[0] * 1200 > 180 & colorSensorFrontArray[1]
		// *
		// 1200 < 50
		// & colorSensorFrontArray[2] * 1200 < 50) {
		// number = 4;
		// }
		//
		// // blue
		// if (colorSensorFrontArray[0] * 1200 < 50 & colorSensorFrontArray[1] *
		// 1200 < 100
		// & colorSensorFrontArray[2] * 1200 > 200) {
		// number = 5;
		// }
	}

	private void goGetThatBlock() {
	}

	private void goLeft() {
		move(20, 120, FORWARD);
	}

	private void goRight() {
		move(120, 20, FORWARD);
	}

	private void grabBlock() {

		move(50, 50, FORWARD);
		timeoutInMs(500);

		move(0, 0, STOP);
		shovel.rotateTo(-165);

	}

	private void increaseBlueCounterAndCheckPosition() {

		dirtyBlueCounter += 1;

		if (dirtyBlueCounter == 5) {

			dirtyBlueCounter = 0;
			setPosition();
			// checkPositionAndActAccordingly();

		}
	}

	public static void move(int leftSpeed, int rightSpeed, int direction) {
		move(leftSpeed, rightSpeed, direction, direction);
	}

	public static void move(int leftSpeed, int rightSpeed, int directionMotorLeft, int directionMotorRight) {

		speedLeft = leftSpeed;
		speedRight = rightSpeed;
		SelectionSort.directionMotorLeft = directionMotorLeft;
		SelectionSort.directionMotorRight = directionMotorRight;

	}

	private void releaseBlock() {
		shovel.rotateTo(0);
	}

	private void scanFront() {

		// if (position == NUMBEROFBLOCKS) {
		// setPosition();
		// }

		move(0, 0, STOP);
		timeoutInMs(500);
		
		grabBlock();

		move(50, 50, BACKWARD);
		timeoutInMs(500);
		move(0, 0, STOP);
		timeoutInMs(500);
		
		if (MainTester.getOutputHandler() != null)
			MainTester.getOutputHandler().sendColor(position, getSortNumberValue(position), (int) (colorSensorFrontArray[0] * 1900),
					(int) (colorSensorFrontArray[1] * 3100), (int) (colorSensorFrontArray[2] * 1600));
		MainTester.sendLog(String.valueOf(
				colorSensorFrontArray[0] * 2000 + colorSensorFrontArray[1] * 2000 + colorSensorFrontArray[2] * 2000));
		// LCD.drawString(
		// "position:" + position + "-red:" + (int) (colorSensorFrontArray[0] *
		// 1200) + "-green:"
		// + +(int) (colorSensorFrontArray[1] * 1200) + "-blue:" + (int)
		// (colorSensorFrontArray[2] * 1200),
		// 0, 5);

		LCD.drawString("#red: " + colorSensorFrontArray[0] * 1200, 0, 5);
		LCD.drawString("#green: " + colorSensorFrontArray[1] * 1200, 0, 6);
		LCD.drawString("#blue: " + colorSensorFrontArray[2] * 1200, 0, 7);

		LCD.drawString("Number: " + getSortNumberValue(position), 0, 8);

		releaseBlock();

	}

	private void setPosition() {

		if (position < NUMBEROFBLOCKS + 2) {
			position++;
		} else {
			position = 0;
		}

	}

	private void timeoutInMs(int ms) {

		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}